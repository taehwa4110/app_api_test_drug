import 'package:app_api_test_drug/config/config_api.dart';
import 'package:app_api_test_drug/model/drug_response.dart';
import 'package:dio/dio.dart';

import '../model/drug_list.dart';

class RepoDrugList {
  final String _baseUrl = '$apiDrug/1471000/DayMaxDosgQyByIngdService/getDayMaxDosgQyByIngdInq';

  Future<DrugList> getList({int page = 1, String CPNT_CD = '', String DRUG_CPNT_KOR_NM = '', String DRUG_CPNT_ENG_NM = '' }) async {
    Map<String, dynamic> params = {};
    params['pageNo'] = page;
    params['type'] = 'json';
    params['serviceKey'] = Uri.encodeFull(apiDrugKey);
    if (CPNT_CD != '') params['CPND_CD'] = Uri.encodeFull(CPNT_CD);
    if (DRUG_CPNT_KOR_NM != '') params['DRUG_CPNT_KOR_NM'] = Uri.encodeFull(DRUG_CPNT_KOR_NM);
    if (DRUG_CPNT_ENG_NM != '') params['DRUG_CPNT_ENG_NM'] = Uri.encodeFull(DRUG_CPNT_ENG_NM);

    Dio dio = Dio();

    final response = await dio.get(
        _baseUrl,
      queryParameters: params,
      options: Options(
        followRedirects: false,
      )
    );

    DrugResponse resultModel = DrugResponse.fromJson(response.data);
    return resultModel.body!;
  }
}