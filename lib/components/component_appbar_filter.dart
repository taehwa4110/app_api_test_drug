import 'package:flutter/material.dart';

class ComponentAppbarFilter extends StatefulWidget implements PreferredSizeWidget {
  const ComponentAppbarFilter({super.key, required this.title, required this.actionIcon, required this.callback});

  final String title;
  final IconData actionIcon;
  final VoidCallback callback;

  @override
  State<ComponentAppbarFilter> createState() => _ComponentAppbarFilterState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(40);
  }
}

class _ComponentAppbarFilterState extends State<ComponentAppbarFilter> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      title: Text(widget.title),
      elevation: 1.0,
      actions: [
        IconButton(onPressed: widget.callback, icon: Icon(widget.actionIcon)),
      ],
    );
  }
}
