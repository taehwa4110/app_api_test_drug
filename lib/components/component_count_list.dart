import 'package:app_api_test_drug/model/drug_list_item.dart';
import 'package:flutter/material.dart';

class ComponentCountList extends StatelessWidget {
  const ComponentCountList({super.key, required this.item, required this.callback});

  final DrugListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(top:10, right:10, left: 10, bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: const Color.fromRGBO(100, 100, 100, 100)),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage('assets/drug.png')
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('성분코드: ${item.CPNT_CD == "" ? '성분코드 데이터가 없습니다' : item.CPNT_CD}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
            Text('성분명(한글): ${item.DRUG_CPNT_KOR_NM == "" ? '성분명(한글) 데이터가 없습니다' : item.DRUG_CPNT_KOR_NM}'),
            Text('성분명(영문): ${item.DRUG_CPNT_ENG_NM == "" ? '성분명(영어) 데이터가 없습니다' : item.DRUG_CPNT_ENG_NM}'),
            Text('제형코드: ${item.FOML_CD == "" ? '제형코드 데이터가 없습니다' : item.FOML_CD}'),
            Text('제형명: ${item.FOML_NM == "" ? '제형명 데이터가 없습니다' : item.FOML_NM}'),
            Text('투여경로: ${item.DOSAGE_ROUTE_CODE == "" ? '투여 경로 데이터가 없습니다' : item.DOSAGE_ROUTE_CODE}'),
            Text('투여단위: ${item.DAY_MAX_DOSG_QY_UNIT == "" ? '투여 단위 데이터가 없습니다' : item.DAY_MAX_DOSG_QY_UNIT}'),
            Text('1일최대투여량: ${item.DAY_MAX_DOSG_QY == "" ? '1일 최대 투여량 데이터가 없습니다' : item.DAY_MAX_DOSG_QY}'),
          ],
        ),
      )
    );
  }
}
