import 'package:flutter/material.dart';

class ComponentsNoContents extends StatelessWidget {
  const ComponentsNoContents({super.key, required this.icon, required this.msg});

  final IconData icon;
  final String msg;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            size: 60,
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            msg,
            style: TextStyle(fontSize: 15),
          ),
        ],
      ),
    );
  }
}
