import 'package:app_api_test_drug/model/drug_list_item.dart';


class DrugList {
  int currentPage;
  int totalPage;
  int totalItemCount;
  List<DrugListItem> items;

  DrugList(
      this.currentPage,
      this.totalPage,
      this.totalItemCount,
      this.items
      );

  factory DrugList.fromJson(Map<String, dynamic> json) {
    int totalItemCount = json['totalCount'] == null ? 0 : json['totalCount'] as int;
    int numOfRows = json['numOfRows'] == null ? 10 : json['numOfRows'] as int;
    int totalPage = (totalItemCount/numOfRows).ceil();

    return DrugList(
      json['pageNo'] == null ? 1 : json['pageNo'] as int, //as 형 변환 json은 String형태 근데 currentPage는 int이므로 형변환이 필요
      (json['totalCount'] != null && json['numOfRows'] != null) ? totalPage : 1,
      json['totalCount'] == null ? 1 : json['totalCount'] as int,
      json['items'] == null ? [] : (json['items']as List).map((e) => DrugListItem.fromJson(e)).toList()
    );
  }
}