import 'package:app_api_test_drug/components/component_appbar_filter.dart';
import 'package:app_api_test_drug/components/component_count_list.dart';
import 'package:app_api_test_drug/components/component_custom_loading.dart';
import 'package:app_api_test_drug/components/component_no_contents.dart';
import 'package:app_api_test_drug/components/component_count_title.dart';
import 'package:app_api_test_drug/pages/page_search.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

import '../model/drug_list_item.dart';
import '../repository/repo_drug_list.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> with AutomaticKeepAliveClientMixin { //한번 뿌렸던 화면정보 저장
  final _scrollController = ScrollController();

  List<DrugListItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  String _CPNT_CD = ''; //검색 필터에 대한 정보 //이것을 알고있어야지 Repository와 교류가 가능합니다. //기본값만 줌
  String _DRUG_CPNT_KOR_NM = ''; //검색 필터에 대한 정보
  String _DRUG_CPNT_ENG_NM = ''; //검색 필터에 대한 정보

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems();
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _currentPage = 1;
      _totalPage = 1;
      _totalItemCount = 0;
  }

    if (_currentPage <= _totalPage) {

      BotToast.showCustomLoading(toastBuilder: (cancelFunc){
        return ComponentCustomLoading(cancelFunc: cancelFunc);
      });

      await RepoDrugList()
          .getList(page: _currentPage, CPNT_CD: _CPNT_CD, DRUG_CPNT_KOR_NM: _DRUG_CPNT_KOR_NM, DRUG_CPNT_ENG_NM: _DRUG_CPNT_ENG_NM)
          .then((res) {
            BotToast.closeAllLoading();

        setState(() {
          _totalPage = res.totalPage;
          _totalItemCount = res.totalItemCount;

          _list = [..._list, ...res.items];

          _currentPage++;
        });
      }).catchError((err) => BotToast.closeAllLoading());
    }

    if (reFresh) {
      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut); // 초기화를 하고 스크롤을 맨위로 올린다
  }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarFilter(
        title: '의약품 정보',
        actionIcon: Icons.search,
        callback: () async { //검색페이지의 동작이 끝날 때 까지 기다린다
          final searchResult = await Navigator.push( //새 창을 열면서 기다립니다
              context,
              MaterialPageRoute(builder: (context) => PageSearch(
                CPNT_CD: _CPNT_CD,
                DRUG_CPNT_KOR_NM: _DRUG_CPNT_KOR_NM,
                DRUG_CPNT_ENG_NM: _DRUG_CPNT_ENG_NM,
              )
              )
          );

          if (searchResult != null && searchResult[0]) { // 넘겨준 값이 null아니거나 첫번째 말이 참이면 검색페이지에서 뭔가 가져가라고 말을 해주었구나라고 느끼고 변수들에 값들을 넣습니다.
            _CPNT_CD = searchResult[1];
            _DRUG_CPNT_KOR_NM = searchResult[2];
            _DRUG_CPNT_ENG_NM = searchResult[3];
            _loadItems(reFresh: true); //_loadItems를 가져오는데 reFresh모드로
          }
        },
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          ComponentCountTitle(icon: Icons.check_circle, count: _totalItemCount, unitName: '건', itemName: '의약품 정보'),
          _buildBody(),
        ],
      ),
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Container(
        decoration: BoxDecoration(color: Colors.grey),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment:  CrossAxisAlignment.stretch,
          children: [
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) => ComponentCountList(item: _list[index], callback: () {}),
            )
          ],
        ),
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 40 - 20,
        child: const ComponentsNoContents(icon: Icons.event_busy, msg: '의약품 정보가 없습니다'),
      );
    }

  }

  @override
  bool get wantKeepAlive => true;
}
