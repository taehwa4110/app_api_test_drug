### 사원 관리 APP
***
(개인프로젝트) 의약품 성분별 1일 최대 투여량을 알려주는 APP
***

### Language
```
Flutter 3.3.2
Dart 2.18.1
```

### 기능
```
* 메인페이지
 - 의약품 리스트 

* 검색 필터

```

### 앱 화면

>* 메인 페이지 화면 (의약품 리스트)
>
>![app-drug-list](./images/app-drug-list.jpg)

>* 검색 화면
>
>![app-search](./images/app-search.jpg)
